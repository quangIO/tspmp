#!/usr/bin/env bash

for n in {2..4}
do
    ./gentsp $n
    perf stat 2>&1 -r 3 -d ./tspmp $1 $n "cities$n.txt">/dev/null | grep -1oP "\d+.\d+ \+" | head -n 1
done
