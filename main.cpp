#include <iostream>
#include <fstream>
#include <cstring>
#include <omp.h>

#ifndef MAX_N
#define MAX_N 24u
#endif

uint32_t distance[MAX_N][MAX_N];
uint32_t dp[1u << MAX_N][MAX_N];
uint8_t trace[1u << MAX_N][MAX_N];
uint8_t path[MAX_N];

// I stole this directly from http://www-graphics.stanford.edu/~seander/bithacks.html
inline uint32_t next_bit_permutation(uint32_t v) {
    uint32_t t = v | (v - 1);
    return (t + 1u) | (((~t & -~t) - 1u) >> (__builtin_ctz(v) + 1u));
}

#include <bitset>

using std::bitset;

inline void run_dp(uint32_t mask, uint32_t n) {
    // std::cout << bitset<32>(mask) << std::endl;
    for (uint32_t i = 0; i < n; ++i) {
        uint32_t prev = mask & ~(1u << i);
        if (prev == mask) continue;
        for (uint32_t j = 0; j < n; ++j) {
            if (!((prev >> j) & 1u)) continue;
            uint64_t cost = dp[prev][j] + distance[i][j];
            if (cost - 1u < dp[mask][i] - 1u) {
                dp[mask][i] = cost;
                trace[mask][i] = j;
            }
        }
    }
}

inline void helper(uint32_t mask, uint32_t end, int thread_id, int num_threads, uint32_t n) {
    for (int i = 0; i < thread_id; ++i) {
        if (mask == end) return;
        mask = next_bit_permutation(mask);
    }
    while (true) {
        run_dp(mask, n);
        for (int i = 0; i < num_threads; ++i) {
            if (mask == end) return;
            mask = next_bit_permutation(mask);
        }
    }
}

int main(int argc, char **argv) {
    std::ios::sync_with_stdio(false);
    int num_threads = static_cast<int>(strtol(argv[1], nullptr, 10));
    auto n = static_cast<uint32_t>(strtol(argv[2], nullptr, 10));
    omp_set_num_threads(num_threads);
    std::ifstream f(argv[3]);
    --n;
    int tmp;
    f >> tmp;
    // memset(dp, 0xF, sizeof(dp));
    for (uint32_t i = 0; i < n; ++i)
        f >> dp[1u << i][i];
    for (int i = 0; i < n; ++i) {
        f >> tmp;
        for (int j = 0; j < n; ++j) f >> distance[i][j];
    }
    for (uint32_t i = 2; i <= n; ++i) { // keep it simple i guess ?
        uint32_t mask = (1u << i) - 1;
        uint32_t end = mask << (n - i);
#pragma omp parallel default(none) shared(mask, end, n)
        {
            helper(mask, end, omp_get_thread_num(), omp_get_num_threads(), n);
        }
    }

    uint32_t result = UINT32_MAX;
    uint32_t end = 0;

    for (uint32_t i = 0; i < n; ++i) {
        if (result > dp[(1u << n) - 1u][i]) {
            result = dp[(1u << n) - 1u][i];
            end = i;
        }
    }

    uint32_t mask = (1u << n) - 1;

    for (int i = 0; i < n; ++i) {
        path[i] = end + 1;
        size_t next = mask & ~(1u << end);
        end = trace[mask][end];
        mask = next;
    }
    std::cout << "Shortest path is:\n";
    std::cout << 0;
    for (int i = 0; i < n; ++i)
        std::cout << ' ' << (int) path[i];
    std::cout << "\ntotal weight = " << result << '\n';
    return 0;
}
